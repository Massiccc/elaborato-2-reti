import sys
import signal
import http.server
import socketserver

def kill_handler(signal, frame):
    print( 'The server is shutting down.')
    try:
      if( server ):
        server.server_close()
    finally:
      sys.exit(0)

#handler per gestire l'interrupt
signal.signal(signal.SIGINT, kill_handler)

#numero di porta predefinito o tramite linea di comando, 
if sys.argv[1:]:
  port = int(sys.argv[1])
else:
  port = 8080

server = socketserver.ThreadingTCPServer(('',port), http.server.SimpleHTTPRequestHandler )

server.daemon_threads = True  

#il Server acconsente al riutilizzo del socket (in caso sovrascrive)
server.allow_reuse_address = True  

# entra nel loop
try:
  while True:
    print("The server is online! Press ctrl + c to stop")
    sys.stdout.flush()
    server.serve_forever()
except KeyboardInterrupt:
  pass

server.server_close()